from flask_sqlalchemy import SQLAlchemy
from flask import Flask,request,jsonify,flash,redirect


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']="sqlite:///employee.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
db = SQLAlchemy(app)

# db.init_app(app)

class Employees(db.Model):
    empname=db.Column(db.String,primary_key=True)
    departmentId=db.Column(db.Integer)

    # def __init__(self,empname,departmentId):
    #     self.empname=empname
    #     self.department=departmentId

    def __repr__(self):
        return f"Employee('{self.empname}','{self.departmentId}')"


class Departments(db.Model):
    departmentId=db.Column(db.Integer,primary_key=True)
    departmentName=db.Column(db.String)
    departmentManager=db.Column(db.String)

    # def __init__(self,departmentId,departmentName,departmentManager):
    #     self.departmentId=departmentId
    #     self.departmentName=departmentName
    #     self.departmentManager=departmentManager

    def __repr__(self):
        return f"Departments('{self.departmentId}','{self.departmentName}','{self.departmentManager}')"


class Incomes(db.Model):
    empname = db.Column(db.String,primary_key=True)
    salary=db.Column(db.Integer)

    # def __init__(self, empname, amount):
    #     self.empname = empname
    #     self.amount = amount
        

    def __repr__(self):
        return f"Incomes('{self.empname}','{self.salary}')"


# Methods

@app.route('/get_emp', methods=['GET'])
def getEmp():
    emp = Employees.query.all()
    a =[]
    for i in emp:
        print("Employee : ", i.empname)
        dept = Departments.query.filter(Departments.departmentId == i.departmentId)
        for y in dept:
            print("Department : ", y.departmentName)
            print("Employee Manager : ", y.departmentManager)
    return "Done"

@app.route('/getDept', methods=['GET'])
def getDepartment():
    dept = Departments.query.all()
    for i in dept:
        print(i.departmentName)
        emp = Employees.query.filter(Employees.departmentId == i.departmentId)
        for j in emp:
            print("Employee Name : ", j.empname)
            print("Employee Manager : ", i.departmentManager)
            print("***")            
    return "Done"

if __name__=="__main__":
    app.run(debug=True)