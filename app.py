from flask_sqlalchemy import SQLAlchemy
from flask import Flask,request,jsonify,flash,redirect


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']="sqlite:///income.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
db = SQLAlchemy(app)

# db.init_app(app)
# with app.app_context():
#     db.create_all()

incomes = [
{
    'description': 'salary','amount':5000,'name':'ayush'
},
{
    'description': 'salary','amount':15000,'name':'gaurav'
},
{
    'description': 'salary','amount':1000,'name':'abhijith'
},
{
    'description': 'salary','amount':1000,'name':'abhijith'
}
]

class Employee(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    empname = db.Column(db.String(20),unique=True,nullable=False)
    income = db.Column(db.Integer,nullable=False)

    def __repr__(self):
        return f"Employee('{self.empname}','{self.income}')"


# Runtime methods
@app.route("/<name>")
def helloWorld(name):
    return f"Hello {name}"

@app.route('/incomes')
def getIncomes():
    return jsonify(incomes)

@app.route('/income/<name>')
def filter_income(name):
    for income in incomes:
        if income.get('name') == name:
            return jsonify(income)
    return "Not Found"

@app.route('/add',methods=['POST'])
def add(name):
    data=request.get_json()
    income.append(data)
    print(incomes)
    return jsonify(data)

@app.route('/delete/<name>',methods=['DELETE'])
def deleteData(name):
    for income in incomes:
        if income.get('name') == name:
            incomes.remove(income)
            print(incomes)
            return "removed Successfully"
    return "Not found"


# Database methods

@app.route('/add',methods=['POST'])
def addEmp():
    data=request.get_json()
    obj=Employee(empname=data["empname"],income=data["income"])
    db.session.add(obj)
    db.session.commit()
    return obj.__repr__()

@app.route('/',methods=['GET'])
def getAll():
    allData=Employee.query.all()
    print(allData)
    return jsonify(allData.__repr__())

@app.route('/delete/<name>', methods=['DELETE'])
def delete(name):
    delete_emp = Employee.query.filter_by(empname=name).first()
    db.session.delete(delete_emp)
    db.session.commit()
    #return "record deleted successfully!"
    return delete_emp.__repr__()



if __name__=="__main__":
    app.run(debug=True)